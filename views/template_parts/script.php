   
    <!-- Mainly scripts -->
    <script src="<?php echo base_url;?>lib/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url;?>lib/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url;?>lib/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url;?>lib/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url;?>lib/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url;?>lib/js/inspinia.js"></script>
    <script src="<?php echo base_url;?>lib/js/plugins/pace/pace.min.js"></script>

