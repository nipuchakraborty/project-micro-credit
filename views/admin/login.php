<!DOCTYPE html>
<!--this page about -->
<html>
<head>

    <?php include_once ("../template_parts/head.php");?>

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">MC+</h1>

        </div>
        <h3>Welcome to Microcradit Company ltd.</h3>
        <p>অনুগ্রহ করে আপনার ইমেইল এবং পাসওয়ার্ড দ্বারা লগইন করুন।</p>
        <form class="m-t" role="form" action="http://webapplayers.com/inspinia_admin-v2.7.1/index.html">
            <div class="form-group">
                <input type="email" class="form-control" placeholder="ব্যাবহার কারীর নাম" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="পাসওয়ার্ড প্রবেশ করুন" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">লগইন করুন</button>

            <a href="#"><small> পাসওয়ার্ড ভুলে গেছেন? </small></a>
            <p class="text-muted text-center"><small>আপনার কি একাউন্ট নেই?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="register.php"> নতুন একাউন্ট তৈরি করুন? </a>
        </form>
        <p class="m-t"> <small>Micro Cradit Company ltd.&copy;<script>document.write(new Date().getFullYear());</script></small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="../../lib/js/jquery-3.1.1.min.js"></script>
<script src="../../lib/js/bootstrap.min.js"></script>

</body>
</html>
