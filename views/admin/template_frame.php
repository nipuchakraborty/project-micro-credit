<!DOCTYPE html>
<html>
<?php
include_once "../../url.php";
?>
<!--head part Start -->
<?php
require_once "../template_parts/head.php";
?>
<!--head part end-->


<!--main body start-->
<body class="">
<!--main Wrapper Start -->
<div id="wrapper">
    <!--Sidebar part-->
    <?php
    include_once "../template_parts/sidebar.php";
    ?>
    <!--sidebar end-->


    <div id="page-wrapper" class="gray-bg">
        <!--header part-->
        <?php
        include_once "../template_parts/header.php";
        ?>
        <!--header part End-->


        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2>This is main title</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index-2.html">This is</a>
                    </li>
                    <li class="active">
                        <strong>Breadcrumb</strong>
                    </li>
                </ol>
            </div>
            <div class="col-sm-8">
                <div class="title-action">
                    <a href="#" class="btn btn-primary">This is action area</a>
                </div>
            </div>
        </div>
        <?php
        include_once ("../template_parts/footer.php");
        ?>

    </div>
</div>
<!--main wrapper end-->



<!-- Mainly scripts -->
<?
include_once ("../template_parts/script.php");
?>
<!--Script include end of tempale-->
</body>
<!--main body end-->
</html>
