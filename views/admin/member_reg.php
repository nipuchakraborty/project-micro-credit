<!DOCTYPE html>
<html>
<?php
include_once "../../url.php";
?>
<!--  include head part -->
<?php
require_once "../template_parts/head.php";
?>

<body class="">

<div id="wrapper">

    <!-- side bar -->
    <?php
    include_once "../template_parts/sidebar.php";
    ?>
    <!-- side bar end -->

    <div id="page-wrapper" class="gray-bg">
        <!-- header  -->
        <?php
        include_once "../template_parts/header.php";
        ?>
        <!-- header end  -->


        <!-- Breadcrumb -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2>সদস্য অর্ন্তভুক্তিকরন ফর্ম</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="#">হোম</a>
                    </li>
                    <li class="active">
                        <strong>সদস্য হালনাগাতকরণ</strong>
                    </li>
                </ol>
            </div>
        </div>
        <!-- Breadcrumb end -->


        <!--////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////-->


        <!-- main section -->
        <div class="wrapper wrapper-content  animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>সদস্য সংযুক্তকরণ তালিকা পূরণ করুন</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a href="javascript:window.location.reload(true)" class="refresh-link">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>


                        <div class="ibox-content" style="">
                            <form role="form" id="form" action="../../src/controller/member_reg.php" method="post"
                                  enctype="multipart/form-data"
                                  novalidate="novalidate">
                                <div id="blockss">
                                    <div class="row">
                                        <div class="info_block">

                                            <!--member information-->
                                            <h4>সদস্য তথ্য</h4>
                                            <div class="divide"></div>

                                            <!--member Name part-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>সদস্যের নাম</label>
                                                    <input type="text" placeholder="নাম" class="form-control"
                                                           name="member_name" id="name" required="">
                                                </div>
                                            </div>

                                            <!--mamber name part end-->


                                            <!--gardian names parts -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>যেকোন একটি নির্বাচন করুন</label> <br>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" class="slectOne" name="father_husband"
                                                               value="1" data-id="1" selected"> পিতা
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" class="slectOne"
                                                                   name="father_husband" value="2" data-id="1" selected"/> স্বামী
                                                        </label>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>পিতার নাম/স্বামীর নাম</label>
                                                    <input type="text" placeholder="পিতার নাম/স্বামীর নাম"
                                                           class="form-control" id="fh_name" name="fh_name" required
                                                           aria-required="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>মাতার নাম</label>
                                                    <input type="text" placeholder="মাতার নাম" class="form-control"
                                                           id="mother_name" name="M_mother_name" required
                                                           aria-required="true">
                                                </div>
                                            </div>
                                            <!--gardian names part end-->


                                            <!--membere age part -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> সদস্যের বয়স</label>
                                                    <input type="number" placeholder="সদস্যের বয়স" class="form-control"
                                                           id="age" name="memberAge" required aria-required="true">
                                                </div>
                                            </div>
                                            <!--member age part end-->


                                            <!--membere ocupation  part start -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> পেশা</label>
                                                    <input type="text" placeholder="সদস্যের পেশা লিখুন" class="form-control"
                                                           id="ocupation" name="ocupation" required aria-required="true">
                                                </div>
                                            </div>
                                            <!--member ocupation part end-->


                                            <!--member nationality part start-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>জাতীয়তা</label>
                                                    <select class="form-control" name="section" id="section" required
                                                            aria-required="true">
                                                        <option value="0">জাতীয়তা নির্বাচন করুন</option>
                                                        <option value="1">Bangladeshi</option>
                                                        <option value="2">NAN</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--member age part end-->


                                            <!--village part Start-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>গ্রাম</label>
                                                    <input type="text" placeholder="আপানার গ্রামের নাম লিখুন" class="form-control"
                                                           id="village" name="village" required aria-required="true">
                                                </div>
                                            </div>


                                            <!--village part end-->

                                            <!--post office start-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>পোষ্ট অফিস</label>
                                                    <input type="text" placeholder="পোষ্ট অফিসের নামে লিখুন"
                                                           class="form-control"
                                                           id="M_post_office" name="post_office" required
                                                           aria-required="true">
                                                </div>
                                            </div>
                                            <!--post office part end-->

                                            <!--upzilla part start -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>উপজেলা</label>
                                                    <select class="form-control" name="M_up_zila" id="up_zila" required
                                                            aria-required="true">
                                                        <option value="">উপজেলা নির্বাচন করুন</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--upzilla part end-->


                                            <!--divistion part start-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>বিভাগ</label>
                                                    <select class="form-control" name="M_division" id="division" required
                                                            aria-required="true">
                                                        <option value="">বিভাগ নির্বাচন করুন</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--division part end-->


                                            <!--zilla part start -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>জেলা</label>
                                                    <select class="form-control" name="M_district" id="district" required
                                                            aria-required="true">
                                                        <option value="">জেলা নির্বাচন করুন</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <!--zilla part end-->

                                            <!--joining date part start-->
                                            <div class="col-lg-3">
                                                <div class="form-group" id="data_1">
                                                    <label>নিবন্ধন তারিখ</label>
                                                    <div class="input-group date">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </span>
                                                        <input name="M_joining_date" id="joining_date" type="text"
                                                               class="form-control" placeholder="মাস/দিন/সাল"
                                                               required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--joining date part end -->

                                            <!--National id no part start-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>জাতীয় পরিচয় পত্রের নং </label>
                                                    <input type="text" placeholder="জাতীয় পরিচয় পত্রের নং লিখুন"
                                                           class="form-control"
                                                           id="M_post_office" name="post_office" required="">
                                                </div>
                                            </div>
                                            <!--national id no part end-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>ছবি যুক্ত করুন</label>
                                                    <input type="file" placeholder="Upload Member Photo"
                                                           class="form-control" name="member_pic" id="member_pic"
                                                           required="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>আঙ্গুলের চাপ সংযুক্ত করুন</label>
                                                    <input type="file" placeholder="Upload Member Fingure-print"
                                                           class="form-control" name="member_fingure"
                                                           id="member_fingure" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <!--nominee information-->
                                    <div class="row">
                                        <div class="info_block">
                                            <h4>নমিনি তথ্য</h4>
                                            <div class="divide"></div>

                                            <div class="col-lg-3">

                                                <div class="form-group">
                                                    <label>মনোনীত ব্যাক্তির নাম</label>
                                                    <input type="text" placeholder="মনোনীত ব্যাক্তির নাম লিখুন"
                                                           class="form-control" name="Nominee_name" id="name" required="">
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>নির্বাচন করুন</label> <br>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" class="slectOne" name="father_husband"
                                                               required="" value="1" data-id="1" selected"/> পিতা
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" class="slectOne"
                                                                   name="father_husband"
                                                                   value="2" data-id="1" selected"/> স্বামী
                                                        </label>
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> মনোনিত ব্যক্তির পিতা/স্বামী</label>
                                                    <input type="text" placeholder="পিতা/স্মামীর নাম লিখুন"
                                                           class="form-control" id="fh_name" name="fh_name" required="">
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>মাতার নাম</label>
                                                    <input type="text" placeholder="মাতার নাম" class="form-control"
                                                           id="mother_name" name="mother_name" required="">
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>মনোনীত ব্যক্তির বয়স</label>
                                                    <input type="number" placeholder="মাতার নাম" class="form-control"
                                                           id="age" name="age" required="">
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>মনোনীত ব্যাক্তির জাতিয়তা</label>
                                                    <select class="form-control" name="section" id="section"
                                                            required="">
                                                        <option value="0">জাতীয়তা নির্বাচন করুন</option>
                                                        <option value="1">Bangladeshi</option>
                                                        <option value="2">NAN</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>মনোনীত ব্যাক্তির সাথে সম্পর্ক</label>
                                                    <select class="form-control" name="section" id="section"
                                                            required="">
                                                        <option value="0">সম্পর্ক নির্বাচন করুন</option>
                                                        <option value="1">বাবা</option>
                                                        <option value="2">মা</option>
                                                        <option value="2">ভাই</option>
                                                        <option value="2">বোন</option>
                                                        <option value="2">স্ত্রী</option>
                                                        <option value="2">সন্তান</option>
                                                        <option value="2">অন্যান্য</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!--membere ocupation  part start -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label> পেশা</label>
                                                    <input type="text" placeholder="সদস্যের পেশা লিখুন" class="form-control"
                                                           id="ocupation" name="ocupation" required aria-required="true">
                                                </div>
                                            </div>
                                            <!--member ocupation part end-->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>গ্রাম</label>
                                                    <input type="text" placeholder="গ্রামের নাম লিখুন"
                                                           class="form-control"
                                                           id="village" name="village" required="true">
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>পোষ্ট অফিস</label>
                                                    <input type="text" placeholder="পোষ্ট অফিসের নাম লিখুন"
                                                           class="form-control"
                                                           id="post_office" name="post_office" required="">
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>উপজেলা</label>
                                                    <select class="form-control" name="up_zila" id="up_zila" required
                                                            aria-required="true">
                                                        <option value="">উপজেলা নির্বাচন করুন</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>জেলা</label>
                                                    <select class="form-control" name="district" id="district"
                                                            required="">
                                                        <option value="">জেলা নির্বাচন করুন</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>বিভাগ</label>
                                                    <select class="form-control" name="division" id="division" required
                                                            aria-required="true">
                                                        <option value="">বিভাগ নির্বাচন করুন</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-3">
                                                <div class="form-group" id="data_1">
                                                    <label>নির্বাচনের তারিখ</label>
                                                    <div class="input-group date">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </span>
                                                        <input name="joining_date" id="joining_date" type="text"
                                                               placeholder="মাস/দিন/সাল"
                                                               class="form-control" value="" required
                                                               aria-required="true">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>জাতীয় পরিচয় পত্রের নং </label>
                                                    <input type="text" placeholder="জাতীয় পরিচয় পত্রের নং লিখুন"
                                                           class="form-control"
                                                           id="post_office" name="post_office" required="">
                                                </div>
                                            </div>
                                            <!--national id no part end-->

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>নমিনির ছবি সংযুক্ত করুন</label>
                                                    <input type="file" placeholder="Upload Member Photo"
                                                           class="form-control" name="member_pic" id="member_pic"
                                                           required
                                                           aria-required="true">
                                                </div>
                                            </div>

                                            <!--fingurePrint upload -->
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>নমিনির আঙ্গুলের চাপ সংযুক্ত করুন</label>
                                                    <input type="file" placeholder="Upload Member Fingure-print"
                                                           class="form-control" name="member_fingure"
                                                           id="member_fingure"
                                                           required aria-required="true">
                                                </div>
                                            </div>
                                            <!--Fingure Print Upload end-->
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-block" type="button" onclick="AddAChild()">আরো নমিনি সংযুক্ত করতে
                                    এইখানে ক্লিক করুন
                                </button>
                                <br>
                                <div class="std_button">
                                    <button class="btn btn-primary m-t-n-xs" type="submit"><strong>সংরক্ষণ করুন</strong>
                                    </button>
                                    <button class="btn btn-warning m-t-n-xs" type="submit"><strong>রিফ্রেস করুন</strong>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <!-- main section ends -->


        <!--////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////-->


        <!-- This is footer -->
        <?php
        include_once "../template_parts/header.php";
        ?>
        <!-- This is the End of Part  -->
    </div>
</div>

<?php require_once "../template_parts/script.php" ?>

<script src="../../lib/js/form_bulider.js" type="text/javascript"></script>


<script>

    $(document).ready(function () {
        $('.slectOne').on('change', function () {
            $('.slectOne').not(this).prop('checked', false);
        });
    });

</script>

</body>

</html>