<!DOCTYPE html>
<html>
<?php
include_once "../../url.php";
?>
<head>
    <?php include("../template_parts/head.php");?>
    <link href="../../lib/css/plugins/dataTables/datatables.min.css" rel="stylesheet">


</head>

<body>

<div id="wrapper">

 <?php include_once ("../template_parts/sidebar.php");?>

    <div id="page-wrapper" class="gray-bg">
        <!--starting of header part-->
        <?php include_once ("../template_parts/header.php");?>
        <!--end of header part-->

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>সদস্য ব্যবস্থাপনা</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index-2.html">হোম</a>
                    </li>
                    <li>
                        <a>সদস্য ব্যবস্থাপনা</a>
                    </li>
                    <li class="active">
                        <strong>সদস্যদের তালিকা</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>সদস্যদের তালিকা</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="member_reg.php">নতুন যুক্ত করুন</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                    <tr>

                                        <th>সদস্যের আইডি</th>
                                        <th>সদস্যের ছবি</th>
                                        <th>সদস্যের নাম</th>
                                        <th>যোগদানের তারিখ</th>
                                        <th>জাতীয় পরিচয় পত্রের নং</th>

                                    </tr>
                                    </thead>


                                    <tbody>
                                    <tr class="gradeX">
                                        <td><?php echo "1"?></td>
                                        <td  style="text-align: center;"><img src="../../lib/img/a1.jpg" alt="" id="p"></td>
                                        <td><?php echo "Mr jhone Doe"?></td>
                                        <td class="center">yy/mm/dd</td>
                                        <td class="center">XXXXXXXXXXXXXXXXXXXXXX</td>
                                    </tr>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>সদস্যের আইডি</th>
                                        <th>সদস্যের ছবি</th>
                                        <th>সদস্যের নাম</th>
                                        <th>যোগদানের তারিখ</th>
                                        <th>ভোটার আইডি নং</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--footer part start-->
      <?php include_once ("../template_parts/footer.php");?>
<!--footer part end-->
    </div>
</div>



<!-- Mainly scripts -->
<script src="../../lib/js/jquery-3.1.1.min.js"></script>
<script src="../../lib/js/bootstrap.min.js"></script>
<script src="../../lib/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="../../lib/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="../../lib/js/plugins/dataTables/datatables.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="../../lib/js/inspinia.js"></script>
<script src="../../lib/js/plugins/pace/pace.min.js"></script>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });

    });

</script>

</body>
</html>
