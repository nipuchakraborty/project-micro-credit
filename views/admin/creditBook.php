<!DOCTYPE html>
<html>

<?php include_once ("../../url.php");?>
<!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Oct 2017 15:31:11 GMT -->
<head>

    <?php include_once ('../template_parts/head.php');?>
    <link href="../../lib/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="../../lib/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

</head>

<body>

<div id="wrapper">

   <?php include_once ('../template_parts/sidebar.php')?>

    <div id="page-wrapper" class="gray-bg">
       <?php include_once ("../template_parts/header.php");?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Data Tables</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index-2.html">Home</a>
                    </li>
                    <li>
                        <a>Tables</a>
                    </li>
                    <li class="active">
                        <strong>Data Tables</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Basic Data Tables example with responsive plugin</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                    <tr>
                                        <th> তারিখ </th>
                                        <th> রশিদ নং </th>
                                        <th> খতিয়ান পৃষ্টা নং </th>
                                        <th> কাহার নিকট হইতে আদায় হলো </th>
                                        <th> আদায় </th>
                                        <th> সম্পাদন করুন </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="gradeU">
                                        <td>Other browsers</td>
                                        <td>All others</td>
                                        <td>-</td>
                                        <td class="center">-</td>
                                        <td class="center">U</td>
                                        <td class=" text-center center"><a href="#" data-toggle="modal" data-target="#myModal2"><i style="font-size: 30px;" class="fa fa-arrow-circle-o-right"></i></a></td>
                                    </tr>

                                    <tr class="gradeU">
                                        <td>Other browsers</td>
                                        <td>All others</td>
                                        <td>-</td>
                                        <td class="center">-</td>
                                        <td class="center">U</td>
                                        <td class=" text-center center"><a href="#" data-toggle="modal" data-target="#myModal2"><i style="font-size: 30px;" class="fa fa-arrow-circle-o-right"></i></a></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th> তারিখ </th>
                                        <th> ভাউচার নং </th>
                                        <th> খতিয়ান পৃষ্টা নং </th>
                                        <th> কাহার দেওয়া হলো </th>
                                        <th> আদায় </th>
                                        <th> সম্পাদন করুন </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
       <?php include_once ('../template_parts/footer.php')?>
        <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated flipInY">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">খরচের বহি</h4>
                        <small class="font-bold">খালি ঘরগুলো পূরণ করে সংরক্ষণ বাটনে ক্লিক করুন </small>
                    </div>
                    <div class="modal-body">
                        <form action="" class="form-group" method="post">
                            <table class="table-bordered">
                                <tbody>
                                <tr>
                                    <td>
                                        <div  id="data_1">
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input type="text" class="form-control" value="03/04/2014">
                                            </div>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input class="form-control" type="text" name="rashidNo" placeholder="রশিদ নং প্রবেশ করান">
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input class="form-control" type="text" name="lager_no" placeholder="খতিয়ানের পৃষ্ঠা নং লিখুন">
                                    </td>
                                </tr>
                                <td></td>
                                <tr>
                                    <td>

                                        <input class="form-control" type="text" name="detor" placeholder="আদায়কৃত ব্যাক্তির নাম">

                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <table>
                                <thead>
                                <tr>
                                    <th>  ফেরত  </th>
                                </tr>
                                </thead>

                               <tbody>
                               <tr>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder="অংশ">
                                   </td>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder="আমানত">
                                   </td>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder="আসল ">
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder=" কর্জ/দাদন ">
                                   </td>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder="ফরম খরিদ টি-এ">
                                   </td>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder="শিক্ষা ফি">
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder="অন্যান্য ">
                                   </td>
                                   <td>
                                       <input  class="form-control" type="text" name="onsho" placeholder="মোট ফেরতের পরিমাণ">
                                   </td>

                               </tr>
                               </tbody>
                            </table>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">বন্ধ করুন</button>
                        <button type="button" class="btn btn-primary"> সংরক্ষণ করুন </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Mainly scripts -->
<?php include_once ("../template_parts/script.php")?>

<script src="../../lib/js/plugins/dataTables/datatables.min.js"></script>
<script src="../../lib/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });

    });


</script>

<script>
    $(document).ready(function () {
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });


        $('.summernote').summernote();

    });
</script>

</body>
</html>
