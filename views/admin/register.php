<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Oct 2017 15:26:54 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Micro Credit Company ltd</title>

    <link href="../../lib/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../../lib/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="../../lib/css/animate.css" rel="stylesheet">
    <link href="../../lib/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">MC+</h1>

        </div>
        <h3>স্বাগতম</h3>
        <p> রেজিস্ট্রশন করুন</p>
        <form class="m-t" role="form" action="http://webapplayers.com/inspinia_admin-v2.7.1/login.html">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="আপনার নাম লিখুন" required="">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="ই-মেইল" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="পাসওয়ার্ড লিখুন" required="">
            </div>
            <div class="form-group">
                <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> আমি কোম্পানির সকল শর্ত মানতে রাজি </label></div>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">রেজিস্টার করুন</button>

            <p class="text-muted text-center"><small>আপনার কি আগে থেকে একটি একাউন্ট আছে?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="login.php">Login</a>
        </form>
        <p class="m-t"> <small>Micro Cradit Company ltd.&copy;<script>document.write(new Date().getFullYear());</script></small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="../../lib/js/jquery-3.1.1.min.js"></script>
<script src="../../lib/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../lib/js/plugins/iCheck/icheck.min.js"></script>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
</body>
</html>
