<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4fb1bfed7b3fb0805fc681dd3b1556a7
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Pondit\\' => 7,
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Pondit\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4fb1bfed7b3fb0805fc681dd3b1556a7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4fb1bfed7b3fb0805fc681dd3b1556a7::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
